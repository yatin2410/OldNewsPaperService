import os
import time
import sys

nme = sys.argv[1]

print(nme)

sfl = os.stat(nme+'.pdf')
sz = sfl.st_size
print(sz)

cnt = 1.1

if sz > 25000000:
    os.system('gs -sDEVICE=pdfwrite -dCompatibilityLevel='+str(cnt)+' -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf '+nme+'.pdf 2>>dump')

cnt = 1.4
if os.stat('output.pdf').st_size > 25000000:
    os.system('gs -sDEVICE=pdfwrite -dCompatibilityLevel='+str(cnt)+' -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf '+nme+'.pdf 2>>dump')

cnt = 1.8
if os.stat('output.pdf').st_size > 25000000:
    os.system('gs -sDEVICE=pdfwrite -dCompatibilityLevel='+str(cnt)+' -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf '+nme+'.pdf 2>>dump')


os.system('rm '+nme+'.pdf 2>>dump')
os.system('mv output.pdf '+nme+'.pdf 2>>dump')
