import os
import bs4
import time
from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup
from PyPDF2 import PdfFileMerger
import urllib


main_url = "http://digitalimages.bhaskar.com/gujarat/epaperpdf/"

if time.localtime(time.time()).tm_mon<10 :
	month = '0'+str(time.localtime(time.time()).tm_mon)
else :
	month = time.localtime(time.time()).tm_mon;

if time.localtime(time.time()).tm_mday<10 :
	date = '0'+str(time.localtime(time.time()).tm_mday)
else :
	date = time.localtime(time.time()).tm_mday;

pdfs = []
id = str(date)+str(month)+'2018'+'/'+str(date-1)+'BARODA%20CITY-PG'

merger = PdfFileMerger()


for i in range(1, 35):
    try:
        back_url = str(id)+str(i)+str('-0')+'.PDF'
        print(main_url+back_url)
        respose = urllib.request.urlopen(main_url+str(back_url))
        pagename = 'dbbarp'+str(i)+'.pdf'
        pdfs.append(pagename)
        fl = open(pagename, 'wb')
        fl.write(respose.read())
        fl.close()
    except:
        for pdf in pdfs:
            merger.append(pdf)
            os.system('rm '+str(pdf)+' 2>>dump')
        try:
            merger.write("dbbar.pdf")
            os.system('python changepdf.py dbbar 2>>dump')
            os.system('python sendpdf.py dbbar 2>>dump')
            break
        except:
            os.system('python senderr.py dbbar 2>>dump')
            break
