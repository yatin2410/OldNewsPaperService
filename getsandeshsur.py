import os
import bs4
import schedule
import time

from urllib.request import Request, urlopen
from bs4 import BeautifulSoup as soup

import urllib

try:
    req = Request('https://sandeshepaper.in/category/22/surat', headers={'User-Agent': 'Mozilla/5.0'})
    page_html = urlopen(req).read()
    page_soup = soup(page_html,"html.parser")
    containers = page_soup.findAll('section',{"class":" col-md-4 col-sm-6 epost epost-home cat text-center"})
    container = containers[0]
    lnk = container.a.get('href')
    lnk = lnk.replace('/edition/','')
    lnk = lnk.replace('/surat','')
    print(lnk)
    req = Request('http://sandeshepaper.in/download/'+str(lnk), headers={'User-Agent': 'Mozilla/5.0'})
    page_html = urlopen(req).read()
    page_soup = soup(page_html,"html.parser")
    containers = page_soup.findAll('p',{"class":"full-page"})
    container = containers[0]
    lnk = container.a.get('href')
    print(lnk)
    os.system('rm sandeshsur.pdf 2>>dump')
    response = urllib.request.urlopen(lnk)
    fl = open('sandeshsur.pdf','wb')
    fl.write(response.read())
    fl.close()
    os.system('python changepdf.py sandeshsur 2>>dump')
    os.system('python sendpdf.py sandeshsur 2>>dump')
except:
    os.system('python senderr.py sandeshsur 2>>dump')
